"use strict"

// TODO: make a WinBox namespace.
import { Space } from './space.js';
import { Win16 } from './win16.js';

window.Space = Space;
window.Win16 = Win16;
