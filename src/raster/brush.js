"use strict";

export class Brush {
    constructor(color) {
        this._color = color;
    }

    get color() {
        return this._color;
    }
}
