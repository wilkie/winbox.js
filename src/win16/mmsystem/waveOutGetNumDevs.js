/**
 * This function retrieves the number of waveform output devices present in the
 * system.
 *
 * @return {Types.UINT} Returns the number of waveform output devices present in
 *                      the system.
 */
export function waveOutGetNumDevs() {
    return 1;
}
